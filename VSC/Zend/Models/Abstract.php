<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Abstract
 *
 * @author Piotr
 */
abstract class Vsc_Zend_Models_Abstract {
    const COLUMN_TYPE_INT = 1;
    const COLUMN_TYPE_FLOAT = 2;
    const COLUMN_TYPE_STRING = 3;
    const COLUMN_TYPE_DATETIME = 4;
    const COLUMN_TYPE_DOUBLE = 5;
    
    /**
     * database table object name
     *
     * @var string
     */
    protected $strDbTable = null;

    /**
     * object database
     *
     * @var Zend_Db_Table_Abstract
     */
    private $objDbTable = null;

    /**
     * model options
     *
     * @var array
     */
    protected $arrOptions = [];

    /**
     * metods
     *
     * @var array
     */
    protected $arrMetods = [];

    /**
     * column aliases
     *
     * @var array
     */
    protected $arrAliases = [];

    /**
     * column aliases return indexes
     *
     * @var array
     */
    private $arrAliasesReturnIndexes = [];

    /**
     * table info
     *
     * @var array
     */
    private $arrInfo = [];

    /**
     * object database row
     *
     * @var Zend_Db_Table_Row_Abstract
     */
    private $objRow = null;

    /**
     * Constructor
     *
     * @return Vsc_Zend_Models_Abstract
     */
    public function __construct($mixId = null) {
        if ($this->strDbTable !== null) {
            if (class_exists($this->strDbTable)) {
                $obj = new $this->strDbTable();
                if ($obj instanceof Zend_Db_Table_Abstract) {
                    $this->objDbTable = $obj;
                    $this->arrInfo = $this->objDbTable->info();
                }
            }
        }
        foreach ($this->arrAliases as $strAlias => $arrAlias) {
            if (array_key_exists('col', $arrAlias)) {
                $this->arrAliasesReturnIndexes[$arrAlias['col']] = $strAlias;
            }
        }
        if ($this->objDbTable instanceof Zend_Db_Table_Abstract && $this->objDbTable->getRowClass() != '' && $this->objDbTable->getRowClass() != 'Zend_Db_Table_Row') {
            $strRowClass = $this->objDbTable->getRowClass();
            if (class_exists($strRowClass)) {
                if ($mixId !== null) {
                    if (!is_int($mixId) && !($mixId instanceof $strRowClass)) throw new Zend_Exception('Bad paramiter $intId must be integer or instanceof '.$strRowClass.' !!!');
                    if ($mixId instanceof $strRowClass) {
                        $this->objRow = $mixId;
                    } elseif (is_int($mixId)) {
                        $this->objRow = $this->getDbTable()->get($mixId);
                        if ($this->objRow === null) {
                            $this->objRow = $this->getDbTable()->createRow();
                            if (!empty($this->arrInfo) && array_key_exists('primary', $this->arrInfo) && array_key_exists(1, $this->arrInfo['primary'])) {
                                $strCol = $this->arrInfo['primary'][1];
                                $this->objRow->$strCol = $mixId;
                            }
                            foreach ($this->arrInfo['metadata'] as $strCol => $arrCol) {
                                if (is_null($this->objRow->$strCol) && is_array($arrCol) && array_key_exists('DEFAULT', $arrCol) && !is_null($arrCol['DEFAULT'])) {
                                    $this->objRow->$strCol = $arrCol['DEFAULT'];
                                }
                            }
                        }
                    }
                } else {
                    $this->objRow = $this->getDbTable()->createRow();
                    foreach ($this->arrInfo['metadata'] as $strCol => $arrCol) {
                        if (is_null($this->objRow->$strCol) && is_array($arrCol) && array_key_exists('DEFAULT', $arrCol) && !is_null($arrCol['DEFAULT'])) {
                            $this->objRow->$strCol = $arrCol['DEFAULT'];
                        }
                    }
                }
            }
        }
        $arrENVOff = ['production', 
                      'development'];
        if (!in_array(APPLICATION_ENV, $arrENVOff)) {
            if (Zend_Registry::isRegistered('bootstrap')) {
                $bootstrap = Zend_Registry::get('bootstrap');
                $arrVscOptions = $bootstrap->getOption('vsc');
                if (is_array($arrVscOptions) && array_key_exists('phpdoc', $arrVscOptions) && array_key_exists('autoupdate', $arrVscOptions['phpdoc']) && $arrVscOptions['phpdoc']['autoupdate']) $this->updatePHPDoc();
            }
        }
    }

    /**
     * get php doc metod desctiption
     *
     * @return string
     */
    private function getPHPDocMetodDesctiption($strMetodName) {
        $strMetodDesctiption = '';
        $arrFromLetters = ['A',  'B',  'C',  'D',  'E',  'F',  'G',  'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',  'P',  'Q',  'R',  'S',  'T',  'U',  'W',  'Z',  'X'];
        $arrToLetters =   [' a', ' b', ' c', ' d', ' e', ' f', ' g', ' h', ' i', ' j', ' k', ' l', ' m', ' n', ' o', ' p', ' q', ' r', ' s', ' t', ' u', ' w', ' z', ' x'];
        $strMetodDesctiption = str_replace($arrFromLetters, $arrToLetters, $strMetodName);
        return $strMetodDesctiption;
    }
    
    /**
     * get php doc
     *
     * @return string
     */
    public function getPHPDoc($strNewLine = '<br />', $strHardPspacebar = '&nbsp') {
        return $this->_getPHPDoc($strNewLine, $strHardPspacebar);
    }
    
    /**
     * get php doc
     *
     * @return string
     */
    private function _getPHPDoc($strNewLine = PHP_EOL, $strHardPspacebar = ' ') {
        $strPHPDoc = '/**';
        $boolFirst = true;
        $strClassName = get_class($this);
        $strRowClass = $this->objDbTable->getRowClass();
        $strPHPDoc .= $strNewLine.$strHardPspacebar.'* @method '.$strClassName.' __construct('.(($this->objDbTable->getRowClass() != '')?($this->objDbTable->getRowClass().'|'):'').'null $mixId) Constructor';
        $strPHPDoc .= $strNewLine.$strHardPspacebar.'*';
        $strPHPDoc .= $strNewLine.$strHardPspacebar.'* @method '.$strRowClass.' getRow() get row object';
        $strPHPDoc .= $strNewLine.$strHardPspacebar.'*';
        $strPHPDoc .= $strNewLine.$strHardPspacebar.'* @method '.$strClassName.' updatePHPDoc() update PHPDoc in this class';
        $strPrimeryCol = '';
        if (!empty($this->arrInfo) && array_key_exists('primary', $this->arrInfo) && array_key_exists(1, $this->arrInfo['primary'])) {
            $strPrimeryCol = $this->arrInfo['primary'][1];
        }
        foreach ($this->arrInfo['metadata'] as $strCol => $arrCol) {
            if (array_key_exists($strCol, $this->arrAliasesReturnIndexes)) {
                $strMetod = $this->arrAliasesReturnIndexes[$strCol];
            } else {
                $strMetod = $strCol;
            }
            if (array_key_exists($strMetod, $this->arrAliases) && array_key_exists('del', $this->arrAliases[$strMetod]) && $this->arrAliases[$strMetod]['del'] == true) {
                continue;
            }
            $strMetodDesctiption = $this->getPHPDocMetodDesctiption($strMetod);
            $strType = 'string';
            $strValName = '$strVal';
            $strValDefault = 'null';
            if (is_array($arrCol) && array_key_exists('DEFAULT', $arrCol) && !is_null($arrCol['DEFAULT'])) {
                $strValDefault = $arrCol['DEFAULT'];
            }
            $strTypeBis = '';
            $strValBisName = '';
            $strValBisDefault = '';
            switch ($this->getColType($this->arrInfo['metadata'][$strCol]['DATA_TYPE'])) {
                case self::COLUMN_TYPE_STRING:
                    $strType = 'string';
                    $strValName = '$strVal';
                    if ($strValDefault !== null && $strValDefault != 'null') $strValDefault = '"'.$strValDefault.'"';
                    break;
                case self::COLUMN_TYPE_INT:
                    $strType = 'int';
                    $strValName = '$intVal';
                    break;
                case self::COLUMN_TYPE_FLOAT:
                    $strType = 'float';
                    $strValName = '$floatVal';
                    break;
                case self::COLUMN_TYPE_DOUBLE:
                    $strType = 'double';
                    $strValName = '$doubleVal';
                    break;
                case self::COLUMN_TYPE_DATETIME:
                    $strType = 'Zend_Date';
                    $strValName = '$mixDateTime';
                    $strTypeBis = 'string';
                    $strValBisName = '$strFormat';
                    $strValBisDefault = '\'dd.MM.YYYY HH:mm:ss\'';
                    break;
                default:
                    $strType = 'string';
                    $strValName = '$strVal';
                    break;
            }
            $strMetodUppetFirstLetter = strtoupper(substr($strMetod, 0, 1)).substr($strMetod, 1);
            $strPHPDoc .= $strNewLine.$strHardPspacebar.'*';
            if ($strCol == $strPrimeryCol) {
                $strPHPDoc .= $strNewLine.$strHardPspacebar.'* @method '.$strType.' _get'.$strMetodUppetFirstLetter.'() get '.$strMetodDesctiption;
            } else {
                $strPHPDoc .= $strNewLine.$strHardPspacebar.'* @method '.$strType.'|null _get'.$strMetodUppetFirstLetter.'() get '.$strMetodDesctiption;
                $strPHPDoc .= $strNewLine.$strHardPspacebar.'* @method '.$strClassName.' _set'.$strMetodUppetFirstLetter.'('.$strType.'|null '.$strValName.(($strValDefault != '')?' = '.$strValDefault:'').(($strTypeBis != '' && $strValBisName != '')?(', '.$strTypeBis.' '.$strValBisName.(($strValBisDefault != '')?(' = '.$strValBisDefault):'')):'').') set '.$strMetodDesctiption;
            }
        }
        $strPHPDoc .= $strNewLine.$strHardPspacebar.'*/';
        return $strPHPDoc;
    }
    
    /**
     * update php doc
     *
     * @return Vsc_Zend_Models_Abstract
     */
    public function updatePHPDoc() {
        $strClass = get_class($this);
        $objReflectionClass = new ReflectionClass($strClass);
        $strFileName = $objReflectionClass->getFileName();
        $strContent = file_get_contents($strFileName);
        $intClassDefStart = strpos($strContent, 'class '.$strClass);
        if (is_writable($strFileName) && is_int($intClassDefStart) && $intClassDefStart > 0) {
            $strNewContent = '<?php'.PHP_EOL.$this->_getPHPDoc(PHP_EOL, ' ').PHP_EOL.substr($strContent, $intClassDefStart);
            file_put_contents($strFileName, $strNewContent);
        }
        return $this;
    }

    /**
     * get magic metod
     *
     * @return mix
     */
    public function __call($strName, $arrParamiters) {
        $strType = substr($strName, 0, 4);
        $strMetod = substr($strName, 4);
        $strMetodLowerFirstLetter = strtolower(substr($strMetod, 0, 1)).substr($strMetod, 1);
        $strCol = null;
        if (array_key_exists($strMetodLowerFirstLetter, $this->arrAliases) && array_key_exists('col', $this->arrAliases[$strMetodLowerFirstLetter]) && array_key_exists($this->arrAliases[$strMetodLowerFirstLetter]['col'], $this->arrInfo['metadata'])) {
            $strCol = $this->arrAliases[$strMetodLowerFirstLetter]['col'];
        } elseif (array_key_exists($strMetodLowerFirstLetter, $this->arrInfo['metadata'])) {
            $strCol = $strMetodLowerFirstLetter;
        }
        $mixValue = null;
        if (!is_null($strCol)) {
            if (($strType == '_get' || $strType == '_set')) {
                switch ($this->getColType($this->arrInfo['metadata'][$strCol]['DATA_TYPE'])) {
                    case self::COLUMN_TYPE_STRING:
                        switch ($strType) {
                            case '_get':
                                $mixValue = $this->getStrProperty($strCol);
                            break;
                            case '_set':
                                if (array_key_exists(0, $arrParamiters)) {
                                    $mixValue = $this->setStrProperty($strCol, $arrParamiters[0]);
                                } else {
                                    $mixValue = $this->setStrProperty($strCol);
                                }
                            break;
                        }
                        break;
                    case self::COLUMN_TYPE_INT:
                        switch ($strType) {
                            case '_get':
                                $mixValue = $this->getIntProperty($strCol);
                            break;
                            case '_set':
                                $mixValue = $this->setIntProperty($strCol, $arrParamiters[0]);
                            break;
                        }
                        break;
                    case self::COLUMN_TYPE_FLOAT:
                        switch ($strType) {
                            case '_get':
                                $mixValue = $this->getFloatProperty($strCol);
                            break;
                            case '_set':
                                $mixValue = $this->setFloatProperty($strCol, $arrParamiters[0]);
                            break;
                        }
                        break;
                    case self::COLUMN_TYPE_DOUBLE:
                        switch ($strType) {
                            case '_get':
                                $mixValue = $this->getDoubleProperty($strCol);
                            break;
                            case '_set':
                                $mixValue = $this->setDoubleProperty($strCol, $arrParamiters[0]);
                            break;
                        }
                        break;
                    case self::COLUMN_TYPE_DATETIME:
                        switch ($strType) {
                            case '_get':
                                $mixValue = $this->getDateTimeProperty($strCol);
                            break;
                            case '_set':
                                if (array_key_exists(0, $arrParamiters) && array_key_exists(1, $arrParamiters)) {
                                    $mixValue = $this->setDateTimeProperty($strCol, $arrParamiters[0], $arrParamiters[1]);
                                } elseif (array_key_exists(0, $arrParamiters)) {
                                    $mixValue = $this->setDateTimeProperty($strCol, $arrParamiters[0]);
                                } else {
                                    $mixValue = $this->setDateTimeProperty($strCol);
                                }
                            break;
                        }
                        break;
                }
            }
        } else {
            throw new Zend_Exception('Metod '.$strName.' not exists !!!');
        }
        return $mixValue;
    }

    /**
     * get column type
     *
     * @return string
     */
    private function getColType($strType) {
        $strReturnType = null;
        if (preg_match('/^((big|medium|small|tiny)?int)/', $strType, $matches)) {
            $strReturnType = self::COLUMN_TYPE_INT;
        } elseif (preg_match('/^(float)/', $strType, $matches)) {
            $strReturnType = self::COLUMN_TYPE_FLOAT;
        } elseif (preg_match('/^(datetime)/', $strType, $matches)) {
            $strReturnType = self::COLUMN_TYPE_DATETIME;
        } elseif (preg_match('/^(double)/', $strType, $matches)) {
            $strReturnType = self::COLUMN_TYPE_DOUBLE;
        } else {
            $strReturnType = self::COLUMN_TYPE_STRING;
        }
        return $strReturnType;
    }

    /**
     * get dbTable object
     *
     * @return Zend_Db_Table_Abstract
     */
    protected function getDbTable() {
        return $this->objDbTable;
    }

    /**
     * get row object
     *
     * @return Zend_Db_Table_Row_Abstract
     */
    protected function getRow() {
        return $this->objRow;
    }

    /**
     * get int property
     *
     * @return int|null
     */
    private function getStrProperty($strName) {
        $strVal = $this->getRow()->$strName;
        return ($strVal !== null)?((string)$strVal):null;
    }

    /**
     * set points
     *
     * @param string|null $strVal
     * @return Vsc_Zend_Models_Abstract
     */
    private function setStrProperty($strName, $strVal = null) {
        if (!is_string($strVal) && !is_null($strVal)) throw new Zend_Exception('Bad paramiter $strVal must be string or null !!!');
        $this->getRow()->$strName = $strVal;
        return $this;
    }

    /**
     * get int property
     *
     * @return int|null
     */
    private function getIntProperty($strName) {
        $intVal = $this->getRow()->$strName;
        return ($intVal !== null)?((int)$intVal):null;
    }

    /**
     * set int property
     *
     * @param int|null $intVal
     * @return Vsc_Zend_Models_Abstract
     */
    private function setIntProperty($strName, $intVal) {
        if (!is_int($intVal) && !is_null($intVal)) throw new Zend_Exception('Bad paramiter $intVal must be integer or null !!!');
        $this->getRow()->$strName = $intVal;
        return $this;
    }

    /**
     * get float property
     *
     * @return float|null
     */
    private function getFloatProperty($strName) {
        $floatVal = $this->getRow()->$strName;
        return ($floatVal !== null)?((float)$floatVal):null;
    }

    /**
     * set float property
     *
     * @param float|null $floatVal
     * @return Vsc_Zend_Models_Abstract
     */
    private function setFloatProperty($strName, $floatVal) {
        if (!is_float($floatVal) && !is_null($floatVal)) throw new Zend_Exception('Bad paramiter $floatVal must be number or null !!!');
        $this->getRow()->$strName = $floatVal;
        return $this;
    }

    /**
     * get double property
     *
     * @return double|null
     */
    private function getDoubleProperty($strName) {
        $floatVal = $this->getRow()->$strName;
        return ($floatVal !== null)?((double)$floatVal):null;
    }

    /**
     * set double property
     *
     * @param double|null $doubleVal
     * @return Vsc_Zend_Models_Abstract
     */
    private function setDoubleProperty($strName, $doubleVal) {
        if (!is_double($doubleVal) && !is_null($doubleVal)) throw new Zend_Exception('Bad paramiter $doubleVal must be number or null !!!');
        $this->getRow()->$strName = $doubleVal;
        return $this;
    }

    /**
     * get int property
     *
     * @return Zend_Date|null
     */
    private function getDateTimeProperty($strName) {
        $strVal = $this->getRow()->$strName;
        return ($strVal !== null)?new Zend_Date($strVal, 'YYYY-MM-dd HH:mm:ss'):null;
    }

    /**
     * set points
     *
     * @param int|null $intData
     * @return Vsc_Zend_Models_Abstract
     */
    private function setDateTimeProperty($strName, $mixVal = null, $strFormat = 'dd.MM.YYYY HH:mm:ss') {
        if ($mixVal instanceof Zend_Date) {
            $this->getRow()->$strName = $mixVal->toString('YYYY-MM-dd HH:mm:ss');
        } elseif (!is_null($mixVal)) {
            $objValidateDate = new Zend_Validate_Date(['format' => $strFormat]);
            if (!$objValidateDate->isValid($mixVal)) throw new Zend_Exception('Bad paramiter $mixVal must be string in format "'.$strFormat.'" or instance of Zend_Date !!!');
            $objCreateDateTime = new Zend_Date($mixVal, $strFormat);
            $this->getRow()->$strName = $objCreateDateTime->toString('YYYY-MM-dd HH:mm:ss');
        } else {
            $this->getRow()->$strName = null;
        }
        return $this;
    }
    
    /**
     * save record
     *
     * @return Vsc_Zend_Models_Abstract
     * @throws Zend_Exception
     */
    public function save() {
        $this->getRow()->save();
        return $this;
    }
    
    /**
     * refresh record
     *
     * @return Vsc_Zend_Models_Abstract
     * @throws Zend_Exception
     */
    public function refresh() {
        $this->getRow()->refresh();
        return $this;
    }

    /**
     * Returns the column/value data as an array.
     *
     * @return array
     */
    public function toArray() {
        $arrData = [];
        $arrDataRow = $this->getRow()->toArray();
        foreach ($arrDataRow as $strCol => $strValue) {
            if (array_key_exists($strCol, $this->arrAliasesReturnIndexes)) {
              $strAlias = $this->arrAliasesReturnIndexes[$strCol];
              if (array_key_exists($strAlias, $this->arrAliases) && is_array($this->arrAliases[$strAlias]) && array_key_exists('del', $this->arrAliases[$strAlias]) && $this->arrAliases[$strAlias]['del'] == true) continue;
              $arrData[$strAlias] = $strValue;
            } else {
              $arrData[$strCol] = $strValue;
            }
        }
        return $arrData;
    }
}
