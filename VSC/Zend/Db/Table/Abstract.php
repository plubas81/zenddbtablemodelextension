<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Abstract
 *
 * @author Piotr
 */
class Vsc_Zend_Db_Table_Abstract extends Zend_Db_Table_Abstract {
    
    protected static $boolTransaction = false;
    protected static $_instance;
    
    /**
     * Updates table rows with specified data based on a WHERE clause.
     *
     * @param  mixed        $table The table to update.
     * @param  array        $bind  Column-value pairs.
     * @param  mixed        $where UPDATE WHERE clause(s).
     * @return int          The number of affected rows.
     * @throws Zend_Db_Adapter_Exception
     */
    public function updateTable($table, array $bind, $where = '') {
        foreach ($bind as &$value) {
            if ($value instanceof Zend_Db_Expr) continue;
            if ($value === null) {
                $value = new Zend_Db_Expr('null');
                continue;
            }
            if (is_number($value) and (int)$value == $value) {
                $value = (int)$value;
                continue;
            }
        }
        return $this->getAdapter()->update($table, $bind, $where);
    }
    
    /**
     * Inserts a table row with specified data.
     *
     * @param mixed $table The table to insert data into.
     * @param array $bind Column-value pairs.
     * @return int The number of affected rows.
     * @throws Zend_Db_Adapter_Exception
     */
    public function insertTable($table, array $bind) {
        foreach ($bind as &$value) {
            if ($value instanceof Zend_Db_Expr) continue;
            if ($value === null) {
                $value = new Zend_Db_Expr('null');
                continue;
            }
            if (is_number($value) and (int)$value == $value) {
                $value = (int)$value;
                continue;
            }
        }
        return $this->getAdapter()->insert($table, $bind);
    }
    
    /**
     * Inserts a table row with specified data.
     *
     * @param mixed $table The table to insert data into.
     * @param array $bind Column-value pairs.
     * @return int The number of affected rows.
     * @throws Zend_Db_Adapter_Exception
     */
    public function deleteFromTable($table, $where) {
        return $this->getAdapter()->delete($table, $where);
    }
    
    /**
     * Get last insert id to db.
     *
     * @return int The number of last insert id.
     * @throws Zend_Db_Adapter_Exception
     */
    public function getLastInsertId() {
        return $this->getAdapter()->lastInsertId();
    }

    static public function isBeginTransaction() {
        return self::$boolTransaction;
    }
    
    static public function beginTransaction() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        self::$_instance->getAdapter()->beginTransaction();
        self::$boolTransaction = true;
    }
    
    static public function commit() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        self::$_instance->getAdapter()->commit();
        self::$boolTransaction = false;
    }
    
    static public function rollBack() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        self::$_instance->getAdapter()->rollBack();
        self::$boolTransaction = false;
    }
}
